import { NgModule } from '@angular/core';

// Import Material Modules Here
import {
  MatToolbarModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatButtonModule,
  MatIconModule,
  MatExpansionModule,
  MatSidenavModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatSelectModule,
  MatMenuModule,
  MatCheckboxModule,
  MatCardModule,
  MatSlideToggleModule,
  MatRadioModule,
  MatGridListModule
} from '@angular/material';

// Add the Imported Modules Here
const modules = [
  MatToolbarModule,
  MatButtonModule,
  MatInputModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatSidenavModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatSelectModule,
  MatMenuModule,
  MatCheckboxModule,
  MatCardModule,
  MatGridListModule,
  MatSlideToggleModule,
  MatRadioModule,
  MatGridListModule
];

@NgModule({
  imports: modules,
  declarations: [],
  exports: modules
})
export class MaterialModule { }
