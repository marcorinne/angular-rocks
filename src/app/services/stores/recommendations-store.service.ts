/* angular */
import { Injectable } from '@angular/core';

/* application */
import { Store } from '../../models/states/store.model';
import { RecommendationsState } from '../../models/states/recommendations-state.model';
import { Product } from '../../models/product';

@Injectable()
export class RecommendationsStoreService extends Store<RecommendationsState> {
    constructor() {
        super(new RecommendationsState());
    }

    getSelectedRecommendation(): Product {
        return this.state.selectedRecommendation;
    }

    setSelectedRecommendation(recommendation: Product): void {
        this.setState({
            ...this.state,
            selectedRecommendation: recommendation
        });
    }

    getRecommendations(): Product[] {
        return this.state.recommendations;
    }

    setRecommendations(recommendations: Product[]): void {
        this.setState({
            ...this.state,
            recommendations: recommendations
        });
    }
}
