import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { ProductService } from '../product/product.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  dashboardObservable: any;
  private cardSelectionSubject: BehaviorSubject<any> = new BehaviorSubject({});
  public cardSelection: Observable<any> = this.cardSelectionSubject.asObservable();
  private onlyMyActivitySubject: BehaviorSubject<any> = new BehaviorSubject(false);
  public onlyMyActivity: Observable<any> = this.onlyMyActivitySubject.asObservable();

  excluded = [
    "__v",
    "anlagedatumTS",
    "leafId",
    "_id",
    "comments",
    "decisionTreeClassification",
    "graphQl_id",
    "name",
    "svmClassification",
    "blacklisted"
  ];
  initVisible = [
    "cagr_wgr",
    "salesqtyTotal",
    "salesqtyTotalPre1",
    "salesqtyTotalPre2",
    "marginTotal",
    "revTotal",
    "copRatio",
  ]

  constructor(private productService: ProductService) {
    this.dashboardObservable = new Observable((observer) => {
      productService.productsObservable.subscribe((products) => {
          if(!Object.keys(this.cardSelectionSubject.getValue()).length){
            const anyProduct = products[0];
            let initSelection = {};
            for(const key of Object.keys(anyProduct)){
              if(!this.excluded.includes(key)){
                initSelection[key] = {
                  visible: this.initVisible.includes(key),
                  nicename: this.getNiceName(key)
                }
              }
            }
            this.cardSelectionSubject.next(initSelection);
          }
          const statistics = this.createStatistics(products);
          observer.next(statistics);
      })
    })
  }

  toggleMyActivity(value) {
    this.onlyMyActivitySubject.next(value);
  }

  getNiceName(ugly: string) {
    const nicename = this.productService.columnDefs.filter((col) => {
      return col.field === ugly;
    })[0].headerName;
    return nicename;
  }

  toggleCard(card) {
    let newSelection = this.cardSelectionSubject.getValue();
    newSelection[card.key].visible = !newSelection[card.key].visible;
    this.cardSelectionSubject.next(newSelection);
  }

  createStatistics(products) {
    // Initialize Boolean Statistics
    let phasedOut = {
      key: "phasedout",
      nicename: "Phased-Out",
      type: "doughnut",
      data: {
        isPhasedOut: 0,
        isInPhaseOutProcess: 0,
        isNotPhasedOut: 0
      }      
    };
    let hasSuccessor = {
      key: "hasSuccessor",
      nicename: "Has Successor",
      type: "doughnut",
      data: {
        "Yes": 0,
        "No": 0
      }
    };
    // Initialize Recent Activity and Trending Statistics
    let mostComments = [];
    let mostCommentsPrev = [];
    let recentActivity = [];
    // Initialize Numeric Statistics (by saving min and max values for every numeric attribute)
    const numericStatistics = this.createNumericStatistics(products);    
    
    for(let i = 0; i < products.length; i++){
      // Boolean Statistics (Count occurrences)
      if(products[i].hasSuccessor === "Yes"){
        hasSuccessor.data["Yes"]++;
      }
      else {
        hasSuccessor.data["No"]++;
      }
      if(products[i].phasedOut === "Yes"){
        phasedOut.data.isPhasedOut++;
      }
      else if(products[i].inPhaseOut === "Yes"){
        phasedOut.data.isInPhaseOutProcess++;
      }
      else {
        phasedOut.data.isNotPhasedOut++;
      }

      // Numeric Statistics (Fill Buckets)
      for(let j in numericStatistics){
        const key = numericStatistics[j].key;
        for(let bucket in numericStatistics[j].buckets) {
          const currentLowerBound = numericStatistics[j].buckets[bucket].lowerBound;
          const currentUpperBound = numericStatistics[j].buckets[bucket].upperBound;
          const currentBucket = numericStatistics[j].buckets[bucket];
          if((currentLowerBound <= products[i][key]) && (products[i][key] <= currentUpperBound)){
            currentBucket.count++;
            break;
          }
        }
      }

      // Trending Statistics
      this.updateMostComments(products[i], mostComments, mostCommentsPrev);

      // Recent Activity Statistics
      this.updateRecentActivity(products[i], recentActivity);
    }

    // Compare the mostComments lists for both periods to find the trends (updates mostComments Array)
    this.getTrends(mostComments, mostCommentsPrev);

    // Calculate Percentages for Numeric Statistics
    for(let i in numericStatistics){
      for(let bucket in numericStatistics[i].buckets){
        const currentBucket = numericStatistics[i].buckets[bucket];
        currentBucket.percentage = currentBucket.count / products.length;
      }
    }

    return {
      charts: [ 
        ...numericStatistics,
        phasedOut,
        hasSuccessor
      ],
      trending: mostComments.slice(0,10),
      recentActivity,
      overview: phasedOut
    };
  }

  createNumericStatistics (products) {    
    // 1. Select all relevant columns and save them in the Object numericStatistics
    let numericStatistics = [];
    const anyProduct = products[0];
    let medianInfo = {};
    for(const key of Object.keys(anyProduct)){
      if(!this.excluded.includes(key) && ((typeof anyProduct[key]) === "number")){
        numericStatistics.push({key, nicename: this.getNiceName(key), type: "barchart"});
        medianInfo[key] = [];
      }
    }
    // 2. Calculate the Median and Min/Max for every attribute    
    for(let product in products) {
      this.updateMedianArray(medianInfo, products[product]);
      this.updateMinMax(numericStatistics, products[product]);
    }    
    for(const medianKey of Object.keys(medianInfo)){
      for(let i = 0; i < numericStatistics.length; i++) {
        if(numericStatistics[i].key === medianKey){
          numericStatistics[i].median = this.median(medianInfo[medianKey]);
        }
      }
    }
    // 3. If the median equals the min-value => Make a doughnut-chart out of it
    for(let i in numericStatistics){
      if(numericStatistics[i].median === numericStatistics[i].min){
        const medianCount = medianInfo[numericStatistics[i].key].filter((number) => {
          return number === numericStatistics[i].median;
        }).length;
        let data = {};
        data["" + numericStatistics[i].median] = medianCount;
        data["> " + numericStatistics[i].median] = products.length - medianCount;
        numericStatistics[i] = {
          ...numericStatistics[i],
          type: "doughnut",
          data
        }
      }
    }

    // 4. Calculate Buckets for every numeric attribute on the basis of the Median value
    this.createMedianBuckets(numericStatistics);

    return numericStatistics;
  }

  createMedianBuckets(numericStatistics){
    const NUMBER_OF_BUCKETS = 5;
    for(let i in numericStatistics){
      if(numericStatistics[i].type === "barchart"){
        const min = numericStatistics[i].min;
        const max = numericStatistics[i].max;
        const median = numericStatistics[i].median;
        const stepReference = (median - min) < (max - median)
          ? (median - min)
          : (max - median);
        const stepSize = Math.floor(stepReference * (2 / NUMBER_OF_BUCKETS));
        const start = median - (stepSize * ((NUMBER_OF_BUCKETS-1) / 2)) - (stepSize / 2); // Only works if #buckets is uneven!
        numericStatistics[i].buckets = [];
        let currentLowerBound = start;
        let currentUpperBound = currentLowerBound + stepSize - 1;
        for(let j = 0; j < NUMBER_OF_BUCKETS; j++){
          numericStatistics[i].buckets[j] = {
            lowerBound: (j === 0) ? min : currentLowerBound,
            upperBound: (j === NUMBER_OF_BUCKETS-1) ? max : currentUpperBound,
            count: 0,
            percentage: null
          }
          currentLowerBound = currentUpperBound + 1;
          currentUpperBound = currentLowerBound + stepSize - 1;
        }
      }
    }
  }

  updateMinMax(numericStatistics, product){
    for(let i in numericStatistics){
      const thisKey = numericStatistics[i].key;
      const productValue = product[thisKey];
      if(isNaN(numericStatistics[i].min) || (productValue < numericStatistics[i].min)) {
        numericStatistics[i].min = productValue;
      }
      if(isNaN(numericStatistics[i].max) || (productValue > numericStatistics[i].max)) {
        numericStatistics[i].max = productValue;
      }
    }
  }

  median(arr){
    const mid = Math.floor(arr.length / 2),
      nums = [...arr].sort((a, b) => a - b);
    return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
  };

  updateMedianArray(medianInfo, product){
    for(const key of Object.keys(medianInfo)){
      medianInfo[key].push(product[key]);
    }
  }

  updateRecentActivity(product, recentActivity){    
    for(let comment in product.comments){      
      const thisComment = product.comments[comment];
      const thisCommentDate = moment(thisComment.date);
      let i = 0;
      for(; i < recentActivity.length; i++) {
        if(thisCommentDate.isAfter(recentActivity[i].date)){
          break;         
        }
      }
        recentActivity.splice(i, 0, {date: thisCommentDate, comment: thisComment, product: product});
    }
  }

  /**
   * Update the mostComments lists for this period and the period before with the
   * the give product 
   */
  updateMostComments(product, mostComments, mostCommentsPrev){
    const MOST_COMMENTS_TIMEFRAME = 30;
    // Breakpoint for this period
    const mostCommentsBreakpoint = moment().subtract(MOST_COMMENTS_TIMEFRAME, 'days');
    // Breakpoint for the period before
    const mostCommentsPrevBreakpoint = moment().subtract(MOST_COMMENTS_TIMEFRAME*2, 'days');    
    // Get the # of comments for this product from this period
    let commentCount = product.comments.filter((comment) => {
      const commentDate = moment(comment.date);
      return (commentDate.isAfter(mostCommentsBreakpoint));
    }).length;
    // Find the position for this product in the mostComments array for this period
    if(commentCount > 0) {
      let mostCommentsPosition = 0;
      for(mostCommentsPosition; mostCommentsPosition < mostComments.length; mostCommentsPosition++){
        if(mostComments[mostCommentsPosition].count < commentCount) break;
      }
      if(mostCommentsPosition < 10) {
        mostComments = mostComments.splice(mostCommentsPosition, 0, {product, count: commentCount});
      }
    }
    // Get the # of comments for this product from the period before
    let commentCountPrev = product.comments.filter((comment) => {
      const commentDate = moment(comment.date);
      return ((commentDate.isBefore(mostCommentsBreakpoint)) && (commentDate.isAfter(mostCommentsPrevBreakpoint)));
    }).length;
    // Find the position for this product in the mostComments array of the period before
    if(commentCountPrev > 0) {
      let mostCommentsPositionPrev = 0;
      for(mostCommentsPositionPrev; mostCommentsPositionPrev < mostCommentsPrev.length; mostCommentsPositionPrev++){
        if(mostCommentsPrev[mostCommentsPositionPrev].count < commentCountPrev) break;
      }
      if(mostCommentsPositionPrev < 10) {
        mostCommentsPrev = mostCommentsPrev.splice(mostCommentsPositionPrev, 0, {product, count: commentCountPrev});
      }
    }
  }

  getTrends(mostComments, mostCommentsPrev) {
    for(let i = 0; i < mostComments.length; i++) {
      for(let j = 0; j < mostCommentsPrev.length; j++){
        if(mostComments[i].product.graphQl_id === mostCommentsPrev[j].product.graphQl_id){
          if(i < j){
            mostComments[i].trend = "up";
          }
          else if(i > j) {
            mostComments[i].trend = "down";
          }
          else {
            mostComments[i].trend = "equal";
          }
        }
      }
      if(!mostComments[i].trend){
        mostComments[i].trend = "up";
      }
    }
  }


  // NOT USED: Buckets which span the whole min -> max space
  createBuckets(numericStatistics){
    const NUMBER_OF_BUCKETS = 7;
    for(let numericKey in numericStatistics){
      if(numericStatistics[numericKey].type === "barchart"){
        const min = numericStatistics[numericKey].min;
        const max = numericStatistics[numericKey].max;
        if(!numericStatistics[numericKey].buckets){
          numericStatistics[numericKey].buckets = [];
          const stepSize = Math.floor((max - min) / NUMBER_OF_BUCKETS);
          let currentLowerBound = Math.floor(min);
          let currentUpperBound = currentLowerBound + stepSize - 1;
          for(let i = 0; i < NUMBER_OF_BUCKETS; i++){          
            numericStatistics[numericKey].buckets[i] = {
              lowerBound: currentLowerBound,
              upperBound: (i === NUMBER_OF_BUCKETS-1) ? Math.ceil(max) : currentUpperBound,
              count: 0,
              percentage: null
            }
            currentLowerBound = currentUpperBound + 1;
            currentUpperBound = currentLowerBound + stepSize - 1;
          }
        }
      }
      
    }
  }
}
