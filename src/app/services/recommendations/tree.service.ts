import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject } from 'rxjs';
import { environment } from '@env/environment';


@Injectable()
export class TreeService {

  public initialGraph = 'tree';

  private graphSelection = new BehaviorSubject<string>(this.initialGraph);
  public selectedGraph = this.graphSelection.asObservable();

  constructor(private authService: AuthService, private http: HttpClient) {}


  getTreeStructure() {
    const url = environment.backendURL + '/recommendations/tree-structure';
    const options = {
      headers: new HttpHeaders({ 'Authorization': this.authService.getAuthorizationHeaderValue()})
    };

    return this.http.get(url, options);
  }


  updateSelectedGraph(graph: string) {
    this.graphSelection.next(graph);
  }

}
