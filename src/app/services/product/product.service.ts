import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { SocketService } from '../socket/socket.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private gridApi;
  private gridColumnApi;

  public columnDefs = [
    {
      headerName: 'ID',
      field: 'graphQl_id',
      hide: false
    },
    {
      headerName: 'Name',
      field: 'name',
      hide: false
    },
    {
      headerName: 'Savings Potential (EP)',
      field: 'potExclParts',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: false
    },
    {
      headerName: 'Savings Potential (A)',
      field: 'potAddOn',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: false
    },
    {
      headerName: 'Phased-Out',
      field: 'phasedOut',
      getQuickFilterText: this.noSearch,
      hide: false
    },
    {
      headerName: 'Comments',
      field: 'comments',
      cellRenderer: this.commentsRenderer,
      hide: false
    },
    {
      headerName: 'In Phase-Out Process',
      field: 'inPhaseOut',
      getQuickFilterText: this.noSearch,
      hide: true
    },
    {
      headerName: 'Projected Sales',
      field: 'cagr_wgr',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.numberFormatter,
      hide: true
    },
    {
      headerName: '% Equal Parts',
      field: 'copRatio',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.precentageFormatter,
      hide: true
    },
    // {
    //   headerName: 'Created At',
    //   field: 'anlagedatumTS',
    //   getQuickFilterText: this.noSearch,
    //   valueFormatter: this.dateFormatter,
    // hide: true
    // },
    {
      headerName: 'Has Successor',
      field: 'hasSuccessor',
      getQuickFilterText: this.noSearch,
      hide: true
    },
    {
      headerName: 'Maximal Performance',
      field: 'maxLeistung5030',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.doubleFormatter,
      hide: true
    },
    {
      headerName: 'Minimal Performance',
      field: 'minLeistung5030',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.doubleFormatter,
      hide: true
    },
    {
      headerName: 'Sales 2017',
      field: 'salesqtyTotal',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.numberFormatter,
      hide: true
    },
    {
      headerName: 'Sales 2016',
      field: 'salesqtyTotalPre1',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.numberFormatter,
      hide: true
    },
    {
      headerName: 'Sales 2015',
      field: 'salesqtyTotalPre2',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.numberFormatter,
      hide: true
    },
    {
      headerName: 'Margin 2017',
      field: 'marginTotal',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: true
    },
    {
      headerName: 'Margin 2016',
      field: 'marginTotalPre1',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: true
    },
    {
      headerName: 'Margin 2015',
      field: 'marginTotalPre2',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: true
    },
    {
      headerName: 'Revenue 2017',
      field: 'revTotal',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: true
    },
    {
      headerName: 'Revenue 2016',
      field: 'revTotalPre1',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: true
    },
    {
      headerName: 'Revenue 2015',
      field: 'revTotalPre2',
      getQuickFilterText: this.noSearch,
      valueFormatter: this.currencyFormatter,
      hide: true
    },
    {
      headerName: '# Parts Total',
      field: 'nbDiffParts',
      getQuickFilterText: this.noSearch,
      hide: true
    },
    {
      headerName: '# Equal Parts',
      field: 'nbDiffCOParts',
      getQuickFilterText: this.noSearch,
      hide: true
    },
    {
      headerName: '# Parts (Category A)',
      field: 'nbDiffCOPartsA',
      getQuickFilterText: this.noSearch,
      hide: true
    },
    {
      headerName: '# Parts (Category B)',
      field: 'nbDiffCOPartsB',
      getQuickFilterText: this.noSearch,
      hide: true
    },
    {
      headerName: '# Parts (Category C)',
      field: 'nbDiffCOPartsC',
      getQuickFilterText: this.noSearch,
      hide: true
    },
    {
      headerName: '# Exclusive Parts',
      field: 'nbDiffExclParts',
      getQuickFilterText: this.noSearch,
      hide: true
    }
  ];

  public gridOptions = {
    columnDefs: this.columnDefs,
    rowSelection: 'single',
    onSelectionChanged: () => {
      this.setCurrentProduct(this.gridApi.getSelectedRows()[0]);
    }
  };

  public productsObservable: any;
  public products;

  private currentProductSubject = new BehaviorSubject<any>(null);
  public currentProductObservable = this.currentProductSubject.asObservable();

  constructor(private socketService: SocketService) {
    const socketConnection = socketService.socketConnection;
    this.productsObservable = new Observable((observer) => {
      socketConnection.subscribe(data => {
        if (Object.keys(data).length > 0) {
          this.products = { ...this.products, ...data };
          const productsAsArray = [];
          const currentProduct = this.currentProductSubject.value;

          for (const key of Object.keys(this.products)) {
            // Get product, alter it and push it into the Products Array
            let product = this.products[key];
            product = {
              ...this.products[key],
              phasedOut: (product.phasedOut) ? 'Yes' : 'No',
              inPhaseOut: (product.inPhaseOut) ? 'Yes' : 'No',
              hasSuccessor: (product.hasSuccessor) ? 'Yes' : 'No'
            };
            productsAsArray.push(product);
            if (currentProduct && product.graphQl_id === currentProduct.graphQl_id) {
              this.currentProductSubject.next(product);
            }
          }
          observer.next(productsAsArray);
        }
      });
    });
  }

  setCurrentProduct(product) {
    this.currentProductSubject.next(product);
  }

  deleteCurrentProduct() {
    this.currentProductSubject.next(null);
  }

  updateColumns(changedProduct) {
    this.gridColumnApi.setColumnVisible(changedProduct.field, !this.gridColumnApi.getColumn(changedProduct.field).visible);
    this.resizeTable();
  }

  public resizeTable() {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  initGrid(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.resizeTable();
  }

  currencyFormatter(params) {
    const dollarFormatter = new Intl.NumberFormat('de-DE',
      {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 2
      });
    return dollarFormatter.format(params.value);
  }

  precentageFormatter(params) {
    return params.value + '%';
  }

  dateFormatter(params) {
    return new Date(params.value).toDateString();
  }

  doubleFormatter(params) {
    return (params.value !== 0) ? params.value.toFixed(2) : params.value;
  }

  numberFormatter(params) {
    const numberFormatter = new Intl.NumberFormat();
    return numberFormatter.format(params.value);
  }

  commentsRenderer(params) {
    return params.value.length === 0 ? '-' : '<b>' + params.value.length + '</b>';
  }

  noSearch(params) {
    return '';
  }
}
