import { AuthCallbackComponent } from './auth-callback.component';
import { NgModule } from '@angular/core';
import { FullscreenLogoSpinnerComponent } from './../../components/fullscreen-logo-spinner/fullscreen-logo-spinner.component';

@NgModule({
  declarations: [AuthCallbackComponent, FullscreenLogoSpinnerComponent]
})
export class AuthModule {}
