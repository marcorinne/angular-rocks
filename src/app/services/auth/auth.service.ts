/* angular */
import { Injectable, Injector } from '@angular/core';

/* 3rd party */
import { from as observableFrom, Observable, BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AuthConfig, OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';

/* application */
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  signedInSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private oAuthService: OAuthService, private injector: Injector) {}

  init(authConfig: AuthConfig) {
    if (authConfig) {
      this.oAuthService.configure(authConfig);
    }
    this.oAuthService.setStorage(localStorage);
    this.oAuthService.tokenValidationHandler = new JwksValidationHandler();
    this.oAuthService.setupAutomaticSilentRefresh();

    this.oAuthService.events.pipe(filter((e) => e.type === 'token_received')).subscribe((e) => {
      this.oAuthService.loadUserProfile();
    });
  }

  startAuthentication() {
    observableFrom(this.oAuthService.loadDiscoveryDocument()).subscribe(() => {
      this.oAuthService.initImplicitFlow();
    });
  }

  completeAuthentication() {
    observableFrom(this.oAuthService.loadDiscoveryDocumentAndTryLogin()).subscribe(() => {
      this.oAuthService.loadUserProfile();

      const router = this.injector.get(Router);
      router.navigate(['/']);
    });
  }

  isSignedIn(): Observable<boolean> {
    return this.signedInSubject.asObservable();
  }

  isLoggedIn(): boolean {
    return this.oAuthService.hasValidAccessToken();
  }

  getUsername(): string {
    const claims = this.oAuthService.getIdentityClaims();
    if (!!claims) {
      return claims['name'];
    }
    return '';
  }

  getUserId(): string {
    const claims = this.oAuthService.getIdentityClaims();
    if (!!claims) {
      return claims['preferred_username'];
    }
    return '';
  }

  getRole(): string {
    const claims = this.oAuthService.getIdentityClaims();
    if (!!claims) {
      return claims['role'];
    }
    return '';
  }

  getAuthorizationHeaderValue(): string {
    return `Bearer ${this.oAuthService.getAccessToken()}`;
  }

  logout(): void {
    this.oAuthService.logOut();
    this.signedInSubject.next(false);
  }
}
