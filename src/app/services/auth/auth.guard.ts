/* angular */
import {
    CanActivate,
    CanActivateChild,
    RouterStateSnapshot,
    ActivatedRouteSnapshot,
    Route,
    CanLoad
  } from '@angular/router';
  import { Injectable } from '@angular/core';

  /* 3rd party */
  import { Observable } from 'rxjs';

  /* application */
  import { AuthService } from './auth.service';

  @Injectable()
  export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
    constructor(private authService: AuthService) {}

    canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
      if (this.authService.isLoggedIn()) {
        return true;
      }
      this.authService.startAuthentication();
      return false;
    }

    canActivateChild(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
      return this.canActivate(route, state);
    }

    canLoad(route: Route): boolean {
      return true;
    }
  }
