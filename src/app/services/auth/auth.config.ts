/* 3rd Party */
import { AuthConfig } from 'angular-oauth2-oidc';

/* application */
import { environment } from '@env/environment';

export function getAuthConfig(window): AuthConfig {
  return {
    // Url of the Identity Provider
    issuer: environment.AUTH_API_PATH,

    loginUrl: environment.AUTH_API_PATH + '/connect/authorize',
    logoutUrl: environment.AUTH_API_PATH + '/connect/endsession',
    userinfoEndpoint: environment.AUTH_API_PATH + '/connect/userinfo',

    // URL of the SPA to redirect the user to after login
    redirectUri: window.location.origin + '/auth-callback',
    silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
    postLogoutRedirectUri: window.location.origin,
    responseType: 'id_token token',

    // The SPA's id. The SPA is registerd with this id at the auth-server
    clientId: 'mvc',

    // set the scope for the permissions the client should request
    // The first three are defined by OIDC. The 4th is a usecase-specific one
    scope:
      'openid profile AppRepository Authentication UserWorkspace',
    showDebugInformation: true,
    clearHashAfterLogin: false,
    useIdTokenHintForSilentRefresh: true
  };
}
