/* angular */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';

/* 3rd party */
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/* application */
import { AuthService } from './auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private router: Router, private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.indexOf('connect/token') < 0) {
      request = request.clone({
        setHeaders: {
          Authorization: this.authService.getAuthorizationHeaderValue()
        }
      });
    }

    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // do stuff with response if needed
          }
        },
        (error: any) => {
          if (error instanceof HttpErrorResponse) {
            switch (error.status) {
              case 401:
                this.authService.logout();
                break;
              default:
            }
          }
        }
      )
    );
  }
}
