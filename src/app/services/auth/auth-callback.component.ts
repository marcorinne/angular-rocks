/* angular */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/* application */
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth-callback',
  template: '<app-fullscreen-logo-spinner></app-fullscreen-logo-spinner>',
  styles: ['']
})
export class AuthCallbackComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    if (this.router.url.includes('error')) {
      this.router.navigate(['/']);
    } else {
      this.authService.completeAuthentication();
    }
  }
}
