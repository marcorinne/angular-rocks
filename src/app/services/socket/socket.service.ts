import { Injectable } from '@angular/core';
/* Application */
import { environment } from '@env/environment';
import { AuthService } from '../auth/auth.service';
/* Async data handling */
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private socket;
  public socketConnection;

  constructor(private authService: AuthService) {
    this.socketConnection = <Subject<any>>this.connect()
      .pipe((response: any): any => {
        return response;
      });
   }

   connect(): Subject<MessageEvent> {
    const observable = new Observable<Object>(internalObserver => {
      this.socket = io(environment.backendURL, {
        transportOptions: {
          polling: {
            extraHeaders: {
              'Authorization': this.authService.getAuthorizationHeaderValue()
            }
          }
        }
      });

      this.socket.on('product', (data) => {
        internalObserver.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });

    const observer = {
      next: (data: Object) => {
        this.socket.emit('comment', data);
      }
    };

    return Subject.create(observer, observable);
  }
}
