/* angular */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatListModule, MatButtonModule, MatTableModule, MatDialogModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSnackBarModule } from '@angular/material/snack-bar';

/* 3rd party */
import { OAuthModule } from 'angular-oauth2-oidc';
import { AgGridModule } from 'ag-grid-angular';
import { ChartsModule } from 'ng2-charts';

/* application */
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RecommendationComponent } from './components/recommendation/recommendation.component';
import { ProductsComponent } from './components/products/products.component';
import { SidebarPanelComponent } from './components/sidebar-panel/sidebar-panel.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { getAuthConfig } from './services/auth/auth.config';
import { TokenInterceptor } from './services/auth/token.interceptor';
import { AuthService } from './services/auth/auth.service';
import { AuthModule } from './services/auth/auth.module';
import { RecommendationListComponent } from './components/recommendation/recommendation-list/recommendation-list.component';
import { DecisionTreeComponent } from './components/recommendation/decision-tree/decision-tree.component';
import { ProductDetailsComponent } from './components/recommendation/product-details/product-details.component';
import { RecommendationsStoreService } from '@services/stores/recommendations-store.service';
import { CommentsSidebarComponent } from './components/comments-sidebar/comments-sidebar.component';
import { BlacklistDialogComponent } from './components/recommendation/blacklist-dialog/blacklist-dialog.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BarchartComponent } from './components/barchart/barchart.component';
import { DashboardToprowComponent } from './components/dashboard-toprow/dashboard-toprow.component';
import { DashboardCardsComponent } from './components/dashboard-cards/dashboard-cards.component';
import { DoughnutComponent } from './components/doughnut/doughnut.component';
import { ProductsOverviewComponent } from './components/products-overview/products-overview.component';
import { TreeService } from '@services/recommendations/tree.service';
import { ScatterPlotComponent } from './components/recommendation/scatter-plot/scatter-plot.component';
import { DisplayRecommendationsComponent } from './components/recommendation/display-recommendations/display-recommendations.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RecommendationComponent,
    ProductsComponent,
    SidebarPanelComponent,
    SidebarComponent,
    RecommendationListComponent,
    DecisionTreeComponent,
    ProductDetailsComponent,
    CommentsSidebarComponent,
    BlacklistDialogComponent,
    DashboardComponent,
    BarchartComponent,
    DashboardToprowComponent,
    DashboardCardsComponent,
    DoughnutComponent,
    ProductsOverviewComponent,
    ScatterPlotComponent,
    DisplayRecommendationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    AuthModule,
    FormsModule,
    ReactiveFormsModule,
    OAuthModule.forRoot(),
    AgGridModule.withComponents([]),
    MatListModule,
    FlexLayoutModule,
    MatButtonModule,
    MatTableModule,
    MatSnackBarModule,
    MatDialogModule,
    ChartsModule
  ],
  providers: [ AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    RecommendationsStoreService,
    TreeService
  ],
  bootstrap: [AppComponent],
  entryComponents: [BlacklistDialogComponent]
})

export class AppModule {
  constructor(private authService: AuthService) {
    // As window is not defined, when AuthService is imported,
    // we initialize AuthService on module construction
    this.authService.init(getAuthConfig(window));
  }
}
