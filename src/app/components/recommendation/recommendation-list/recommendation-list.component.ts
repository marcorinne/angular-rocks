/* angular */
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

/* application */
import { Product } from 'app/models/product';

@Component({
  selector: 'app-recommendation-list',
  templateUrl: './recommendation-list.component.html',
  styleUrls: ['./recommendation-list.component.scss']
})
export class RecommendationListComponent implements OnInit, OnChanges {
  @Input() recommendations: Product[];
  @Output() recommendationSelected: EventEmitter<string> = new EventEmitter();

  displayedColumns = ['name'];

  dataSource: MatTableDataSource<any>;

  selectedRowIndex = 0;

  constructor() {}

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.recommendations.firstChange) {
      this.dataSource = new MatTableDataSource<any>(changes.recommendations.currentValue.map(x => x.name));
    }
  }

    highlight(row: string, i: number) {
      this.selectedRowIndex = i;
      this.recommendationSelected.emit(row);
  }
}
