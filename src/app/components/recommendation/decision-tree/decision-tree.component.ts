import { Component, OnChanges, ViewChild, ElementRef, Input } from '@angular/core';
import { Product } from '../../../models/product';
import { TreeService } from '@services/recommendations/tree.service';

import * as d3 from 'd3';

@Component({
  selector: 'app-decision-tree',
  templateUrl: './decision-tree.component.html',
  styleUrls: ['./decision-tree.component.scss']
})
export class DecisionTreeComponent implements OnChanges {

  @ViewChild('container') container: ElementRef;
  @Input() selectedProduct: Product;

  tree;

  // set the dimensions and margins of the diagram
  margin = { top: 10, right: 50, bottom: 0, left: 50 };
  height = 900;

  constructor(private treeService: TreeService) {
  }


  ngOnChanges() {
    this.treeService.getTreeStructure()
      .subscribe(data => {
        this.tree = data;
        this.buildTree();
    });
  }


  buildTree() {
    const leafID = String(this.selectedProduct.leafId);

    // Get width of decision tree component so that the tree can
    // be fit into its container
    const width = (this.container.nativeElement as HTMLElement).offsetWidth;

    const div = d3.select('#svg-container');

    if ((div.nodes()[0] as HTMLElement).children) {
      d3.selectAll('svg').remove();
    }

    const svg = div.append('svg')
      .attr('width', width)
      .attr('height', this.height);


    // Append the clip path
    svg
      .append('clipPath')
      .attr('id', 'clipper')
      .append('rect')
      .attr('id', 'clip-rect');

      const treeWidth = width - this.margin.left;
      const treeHeight = this.height - (this.height / 3);

      const treeLayout = d3.tree().size([treeWidth, treeHeight]);
      const root = d3.hierarchy(this.tree);
      const nodes = treeLayout(root); // Writes x and y values into every node of root


      const g = svg.append('g')
        .attr('transform', 'translate(' + this.margin.left +
          ',' + (this.margin.top + 70) + ')');


      // Insert empty group elements for each path
      const linkGroup = g.selectAll('.link')
        .data(root.descendants().slice(1))
        .enter()
        .append('g')
        .attr('class', 'path-group');


      // Add edges
      const linkPath = linkGroup.append('path')
        .attr('fill', 'none')
        .attr('d', (d: any) => {
          return 'M' + d.x + ',' + d.y
            + 'C' + d.x + ',' + (d.y + d.parent.y) / 2
            + ' ' + d.parent.x + ',' + (d.y + d.parent.y) / 2
            + ' ' + d.parent.x + ',' + d.parent.y;
        });


      // Adds each node as a group
      const node = g.selectAll('.node')
        .data(nodes.descendants())
        .enter().append('g')
        .attr('transform', d => {
          return 'translate(' + (d.x) + ',' + d.y + ')';
        });



      // ---------------- Add shapes for nodes and leafs ----------------

      // Select all nodes (=> not leafs!)
      const innerNodes = node
        .filter((d: any) => {
          if (d.height > 0) {
            return d;
          }
        });


      innerNodes.append('rect')
        .attr('class', 'ellipse')
        .attr('width', 140)
        .attr('height', 50)
        .attr('x', -70)
        .attr('y', -25);


      // Select all the leafs
      const leafs = node
        .filter((d: any) => {
          if (d.height === 0) {
            return d;
          }
        });

      leafs.append('circle')
        .attr('r', 30)
        .attr('class', 'leaf');


      // ---------------- Add labels to nodes/leafs and paths ----------------

      // Add label to paths
      g.selectAll('.path-group')
        .append('text')
        .attr('x', (d: any) => d.x)
        .attr('y', (d: any) => d.y)
        .attr('dy', (d: any) => -(Math.abs(d.y - d.parent.y) / 2)) // vertically center the text on the path
        .attr('dx', (d: any, i: number) => {
          const mean = (Math.abs(d.x - d.parent.x) / 2);
          return this.isEven(i) ? (mean - 10) : (-mean + 10);
        })
        .style('text-anchor', (d: any, i: number) => this.isEven(i) ? 'end' : 'start')
        .text((d: any, i: number) => {
          return this.isEven(i) ? ('> ' + d.parent.data.threshold)
            : ('\u2264 ' + d.parent.data.threshold);
        });


      // Adds the labels to the nodes and leafs
      node.append('text')
        .attr('dy', '.35em')
        .attr('fill', (d: any) => d.data.id === leafID ? 'white' : 'black')
        .style('text-anchor', 'middle')
        .attr('dominant-baseline', 'baseline')
        .text((d: any) => {
          if (d.children) {
            return d.data.name;
          } else {
            return d.data.name === 'phase-out' ? '\u2714' : 'X';
          }
        });



      // ---------------- Add the highlighted path ----------------
      // Select the final leaf
      node.filter((d: any) => d.data.id === leafID)
        .selectAll('circle')
        .attr('class', 'finalLeaf');


      // Build a list of all nodes from root to the
      // currently selected node
      const finalLeaf_ancestors = root.descendants()
        .filter((d: any) => d.data.id === leafID)
        [0]
        .ancestors().map((i) => i.data);


      // Check if path connects two nodes from the
      // finalLeaf_ancestors list
      const isFocusedPath = (d) => {
        const x = finalLeaf_ancestors.filter((a: any) => a.id === d.data.id);
        return !!x[0];
      };


      // Add another path element on top of the already existing one
      // for all sections of the focused path. It's necessary to layer
      // above each other so that there is a "normal" path shown before
      // the highlighted one if fully revealed
      g.selectAll('.path-group')
        .filter((d: any) => isFocusedPath(d))
        .append('path')
        .attr('class', 'second-layer')
        .attr('d', (d: any) => {
          if (d.parent) {
            return 'M' + d.x + ',' + d.y
              + 'C' + d.x + ',' + (d.y + d.parent.y) / 2
              + ' ' + d.parent.x + ',' + (d.y + d.parent.y) / 2
              + ' ' + d.parent.x + ',' + d.parent.y;
          }
        })
        .attr('stroke', '#F7AF26')
        .attr('stroke-width', '4')
        .attr('fill', 'none')
        .attr('clip-path', 'url(#clipper)');


      // Reveal path from top to bottom
      svg.selectAll('#clip-rect')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', treeWidth)
        .attr('height', 0)
        .transition()
        .duration(2000)
        .attr('height', treeHeight);
  }



  // Check if argument is even
  isEven(i: number) {
    if (i % 2 === 0) {
      return true;
    } else {
      return false;
    }
  }

}
