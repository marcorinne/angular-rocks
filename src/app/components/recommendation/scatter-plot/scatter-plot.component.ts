import { Component, OnInit, OnDestroy, Input, ElementRef, ViewChild } from '@angular/core';
import { Product } from '../../../models/product';
import { ProductService } from '../../../services/product/product.service';
import * as d3 from 'd3';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-scatter-plot',
  templateUrl: './scatter-plot.component.html',
  styleUrls: ['./scatter-plot.component.scss']
})

export class ScatterPlotComponent implements OnInit, OnDestroy {

  @ViewChild('scatterContainer') scatterContainer: ElementRef;
  @Input() selectedProduct: Product;

  margin = {top: 50, right: 0, bottom: 0, left: 70};
  height = 500;

  ids: Array<number> = [];
  currentID: String;
  feature = 'marginTotal';
  scales = [
    {
      'id': 1,
      'name': 'linear'
    },
    {
      'id': 2,
      'name': 'square root'
    }
  ];
  yScale = 'square root';
  featureOptions = [
    {
      'name': 'Potential exclusive parts',
      'value': 'potExclParts'
    },
    {
      'name': 'Savings potential',
      'value': 'potAddOn'
    },
    {
      'name': 'Planned sales 2018-2020',
      'value': 'cagr_wgr'
    },
    {
      'name': 'Percentage of identical parts',
      'value': 'copRatio'
    },
    {
      'name': 'Sales quantity',
      'value': 'salesqtyTotal'
    },
    {
      'name': 'Sales quantity 2016',
      'value': 'salesqtyTotalPre1'
    },
    {
      'name': 'Sales quantity 2015',
      'value': 'salesqtyTotalPre2'
    },
    {
      'name': 'Total margin',
      'value': 'marginTotal'
    },
    {
      'name': 'Total margin 2016',
      'value': 'marginTotalPre1'
    },
    {
      'name': 'Total margin 2015',
      'value': 'marginTotalPre2'
    },
    {
      'name': 'Total revenue',
      'value': 'revTotal'
    },
    {
      'name': 'Total revenue 2016',
      'value': 'revTotalPre1'
    },
    {
      'name': 'Total revenue 2015',
      'value': 'revTotalPre2'
    },
    {
      'name': 'Maximal performance',
      'value': 'maxLeistung5030'
    },
    {
      'name': 'Minimal performance',
      'value': 'minLeistung5030'
    },
    {
      'name': '# different parts',
      'value': 'nbDiffParts'
    },
    {
      'name': 'nbDiffCOParts',
      'value': 'nbDiffCOParts'
    },
    {
      'name': '# of A parts',
      'value': 'nbDiffCOPartsA'
    },
    {
      'name': '# of C parts',
      'value': 'nbDiffCOPartsB'
    },
    {
      'name': '# of C parts',
      'value': 'nbDiffCOPartsC'
    },
    {
      'name': '# different exclusive parts',
      'value': 'nbDiffExclParts'
    }
  ];

  index: number;
  decisionTreePhaseOut = true;
  supportVectorPhaseOut = false;
  selectedProductName: String;

  data: any;

  dataAvailable = false;

  productsSubscription: Subscription;

  constructor(private productService: ProductService) {
   }


  ngOnInit() {
    this.productsSubscription = this.productService.productsObservable
    .subscribe((data: Product[]) => {
      console.log('new scatter data');
      this.data = data;
      this.updateScatter();
    });

    this.currentID = this.selectedProduct.graphQl_id;
  }


  ngOnDestroy() {
    this.productsSubscription.unsubscribe();
  }



  updateScatter(): void {
    const div = d3.select('#products-container');

    if ((div.nodes()[0] as HTMLElement).children) {
      d3.selectAll('svg').remove();
    }

    const width = (this.scatterContainer.nativeElement as HTMLElement).offsetWidth;

    const svg = div.append('svg')
      .attr('width', width - this.margin.right)
      .attr('height', this.height + 150);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left +
                                  ',' + this.margin.top + ')');

    // X Axis values
    for (let i = 0; i < this.data.length; i++) {
      this.ids.push(i);
    }

    // Min and max values for both axis
    const yMinValue = this.findMin(this.data, this.feature);
    const yMaxValue = this.findMax(this.data, this.feature);
    const xMinValue = this.findMin(this.ids, 'id');
    const xMaxValue = this.findMax(this.ids, 'id');

    // Set the axis, mapping input to size of charting area
    const x = d3.scaleLinear()
      .domain([xMinValue, xMaxValue])
      .range([0, width]);

    // y starts from the top, so range has to be inverted
    const currentYScale = this.getScale();
    const y = currentYScale
      .domain([yMinValue, yMaxValue])
      .range([this.height, 0]);

    // Add y axis
    g.append('g')
      .attr('id', 'yAxis')
      .call(d3.axisLeft(y));

    // Add the circles
    const circles = g.selectAll('.dot')
      .data(this.data)
      .enter()
      .append('circle')
      .attr('r', d => {
        if (d['graphQl_id'] === this.currentID) {
          return 12;
        } else {
          return 2.5;
        }})
      .attr('cx', (d, i) => x(i))
      .attr('cy', d => y(d[this.feature]))
      .attr('class', d => {
        if (d['decisionTreeClassification'] === true || d['svmClassification'] === true) {
          return 'phasedOut';
        } else {
          return 'notPhasedOut';
        }
      })
      .attr('id', d => {
        if (d['graphQl_id'] === this.currentID) {
          return 'selectedCircle';
        }
      });


    // Add hovering functionality to circles
    circles
      .on('mouseover', function() {
        d3.select(this)
          .transition()
          .duration(200)
          .attr('r', () => (this.id === 'selectedCircle') ? 20 : 10);
      })
      .on('mouseout', function() {
        d3.select(this)
          .transition()
          .duration(200)
          .attr('r', () => (this.id === 'selectedCircle' ? 12 : 2.5));
      })
      .append('title')
        .text((d) => {
          return d['name'] + '\n' + this.feature + ': '
                            + ((Math.round(d[this.feature]) * 100 / 100));
        });

    this.dataAvailable = true;
  }


  updateY(): void {
    // Min and max values for y axis
    const yMinValue = this.findMin(this.data, this.feature);
    const yMaxValue = this.findMax(this.data, this.feature);

    // Redefine y axis scaling
    const currentYScale = this.getScale();
    const y = currentYScale
      .domain([yMinValue, yMaxValue])
      .range([this.height, 0]);

    // Redraw the y axis
    d3.selectAll('#yAxis')
      .transition()
      .duration(700)
      .call(d3.axisLeft(y).bind(this));

    const circles = d3.selectAll('circle');

    // Update data points
    circles
      .transition()
      .duration(700)
      .attr('cy', d => y(d[this.feature]));

    /*
      The old tooltips have to be deleted before
      inserting new ones. Of course, it would be
      possible to just add the new before the old
      ones. Then, however, lots of titles would pile
      up in the DOM when new features are chosen often.
    */
    d3.selectAll('title').remove();

    // Add tooltip functionality on mouseover
    circles
      .on('mouseover', function() {
        d3.select(this)
          .transition()
          .duration(200)
          .attr('r', function(this: SVGCircleElement) {
            return (this.id === 'selectedCircle') ? 20 : 10;
          });

      })
      .on('mouseout', function() {
        d3.select(this)
          .transition()
          .duration(200)
          .attr('r', function(this: SVGCircleElement) {
            return (this.id === 'selectedCircle') ? 12 : 2.5;
          });
      })
      .append('title')
        .text((d) => {
          return d['name'] + '\n' + this.feature + ': '
                          + ((Math.round(d[this.feature]) * 100 / 100));
        });
  }


  // Return the appropriate scale depending on user input
  getScale() {
    if (this.yScale === 'square root') {
      return d3.scaleSqrt();
    } else if (this.yScale === 'linear') {
      return d3.scaleLinear();
    }
  }

  /*
    Find smallest value in 2-dimensional array at provided index
    in 2nd dimension, i.e. [[key, key]]
    When we look for the smallest/highest ID we have to only deal
    with a 1d array, so simpler indexing is required.
  */
  findMin(list, key): number {
    let minValue: number;
    if (key === 'id') {
      minValue = list[0];
      for (let i = 1; i < list.length; i++) {
        if (list[i] < minValue) {
          minValue = list[i];
        }
      }
    } else {
      minValue = list[0][key];
      for (let i = 1; i < list.length; i++) {
        if (list[i][key] < minValue) {
          minValue = list[i][key];
        }
      }
    }
    return minValue;
  }


  findMax(list, key): number {
    let maxValue: number;
    if (key === 'id') {
      maxValue = list[0];
      for (let i = 1; i < list.length; i++) {
        if (list[i] > maxValue) {
          maxValue = list[i];
        }
      }
    } else {
      maxValue = list[0][key];
      for (let i = 1; i < list.length; i++) {
        if (list[i][key] > maxValue) {
          maxValue = list[i][key];
        }
      }
    }
    return maxValue;
  }


}
