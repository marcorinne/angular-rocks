import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayRecommendationsComponent } from './display-recommendations.component';

describe('DisplayRecommendationsComponent', () => {
  let component: DisplayRecommendationsComponent;
  let fixture: ComponentFixture<DisplayRecommendationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayRecommendationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayRecommendationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
