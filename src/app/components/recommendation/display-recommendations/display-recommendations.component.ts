import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'app/models/product';

@Component({
  selector: 'app-display-recommendations',
  templateUrl: './display-recommendations.component.html',
  styleUrls: ['./display-recommendations.component.scss']
})
export class DisplayRecommendationsComponent implements OnInit {

  @Input() selectedProduct: Product;

  constructor() { }

  ngOnInit() { }

}
