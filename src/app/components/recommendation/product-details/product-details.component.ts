/* angular */
import { MatSnackBar, MatTableDataSource } from '@angular/material';
import { Component, OnInit, Input, SimpleChanges, OnChanges, OnDestroy } from '@angular/core';

/* application */
import { Product } from '../../../models/product';
import { SocketService } from '@services/socket/socket.service';

export interface ProductAttributes {
  attribute: string;
  value: string;
}
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() selectedProduct: Product;

  displayedColumns: string[] = ['attribute', 'value'];
  dataSource: MatTableDataSource<ProductAttributes>;
  socketConnection;

  constructor(private snackBar: MatSnackBar, private socketService: SocketService) {
    this.socketConnection = socketService.socketConnection;
  }
  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.selectedProduct.firstChange) {
      this.dataSource = new MatTableDataSource<ProductAttributes>(this.getAttributes(changes.selectedProduct.currentValue));
    }
  }

  ngOnDestroy() {
    this.socketConnection.unsubscribe();
  }

  getAttributes(selectedProduct: Product): ProductAttributes[] {
    return [
      {attribute: 'Number of different parts', value: selectedProduct.nbDiffParts.toString()},
      {attribute: 'Number of different exclusive parts', value: selectedProduct.nbDiffExclParts.toString()},
      {attribute: 'Total margin', value: this.formatCurrency(selectedProduct.marginTotal)},
      {attribute: 'Total revenue', value: this.formatCurrency(selectedProduct.revTotal)},
      {attribute: 'Sales quantity', value: selectedProduct.salesqtyTotal.toString()},
      {attribute: 'Maximal performance', value: this.formatCurrency(selectedProduct.maxLeistung5030)},
      {attribute: 'Minimal performance', value: this.formatCurrency(selectedProduct.minLeistung5030)},
      {attribute: 'Total revenue 2016', value: this.formatCurrency(selectedProduct.revTotalPre1)},
      {attribute: 'Total revenue 2015', value: this.formatCurrency(selectedProduct.revTotalPre2)},
      {attribute: 'Total margin 2016', value: this.formatCurrency(selectedProduct.marginTotalPre1)},
      {attribute: 'Total margin 2015', value: this.formatCurrency(selectedProduct.marginTotalPre2)},
      {attribute: 'Sales quantity 2016', value: selectedProduct.salesqtyTotalPre1.toString()},
      {attribute: 'Sales quantity 2015', value: selectedProduct.salesqtyTotalPre2.toString()},
      {attribute: 'Number of A parts in the product', value: selectedProduct.nbDiffCOPartsA.toString()},
      {attribute: 'Number of B parts in the product', value: selectedProduct.nbDiffCOPartsB.toString()},
      {attribute: 'Number of C parts in the product', value: selectedProduct.nbDiffCOPartsC.toString()}];
  }

  formatCurrency(value: number): string {
    return new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 2
    }).format(value);
  }

  precentageFormatter(params) {
    return params.value + '%';
  }

  openSnackBar() {
    const graphQl_id = this.selectedProduct.graphQl_id;

    /* Open snackbar before requesting back-end */
    this.snackBar.open(`${this.selectedProduct.name} added to Blacklist`, 'Undo', {
      duration: 2500
    });

    this.socketConnection.next({
      method: 'addToBlacklist',
      graphQl_id: graphQl_id,
    });

    /* Subscribe to button click on snackbar (e.g. 'Undo' button) */
    this.snackBar._openedSnackBarRef.onAction().subscribe(() => {
      this.socketConnection.next({
        method: 'deleteFromBlacklist',
        graphQl_id: graphQl_id,
      });
    });
  }
}
