/* angular */
import { Component, OnInit } from '@angular/core';

/* application */
import { RecommendationsStoreService } from '@services/stores/recommendations-store.service';
import { Product } from 'app/models/product';
import { ProductService } from '@services/product/product.service';
import { TreeService } from '@services/recommendations/tree.service';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit {
  recommendations: Product[];
  selectedGraph: string;
  dataAvailable = false;

  constructor(public store: RecommendationsStoreService,
              private productService: ProductService,
              private treeService: TreeService) {
  }

  ngOnInit() {
    this.productService.productsObservable.subscribe((products: Product[]) => {
      this.recommendations = products.filter((product: Product) => {
        return (product.decisionTreeClassification || product.svmClassification) && !product.blacklisted;
      });
      this.store.setRecommendations(this.recommendations);
      this.store.setSelectedRecommendation(this.recommendations[0]);
      this.dataAvailable = true;

    });



    this.treeService.selectedGraph.subscribe(graph => this.selectedGraph = graph);
  }

  onRecommendationSelected(event: string) {
    this.store.setSelectedRecommendation(this.recommendations.find((rec) => rec.name === event));
  }
}
