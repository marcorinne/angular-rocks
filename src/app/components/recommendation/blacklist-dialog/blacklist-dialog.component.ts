/* angular */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Product } from 'app/models/product';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MatTableDataSource } from '@angular/material';

/* application */
import { ProductService } from '@services/product/product.service';
import { SocketService } from '@services/socket/socket.service';

export interface BlacklistDataSource {
  materialnr: string;
  name: string;
  revenue: string;
  margin: string;
  salesqnt: number;
}

@Component({
  selector: 'app-blacklist-dialog',
  templateUrl: './blacklist-dialog.component.html',
  styleUrls: ['./blacklist-dialog.component.scss']
})
export class BlacklistDialogComponent implements OnInit, OnDestroy {
  blacklistedProducts: Product[];
  displayedColumns: string[] = ['select', 'materialnr', 'name', 'revenue', 'margin', 'salesqnt'];
  blacklistDataSource: MatTableDataSource<BlacklistDataSource>;
  selection = new SelectionModel<BlacklistDataSource>(true, []);
  socketConnection: any;

  dataAvailable = false;

  constructor(private productService: ProductService, public dialogRef: MatDialogRef<BlacklistDialogComponent>,
     private socketService: SocketService) {
    this.socketConnection = socketService.socketConnection;

    this.productService.productsObservable.subscribe((products: Product[]) => {
      const prs = products.filter((product: Product) => {
        this.dataAvailable = true;
        return (product.decisionTreeClassification || product.svmClassification) && product.blacklisted;
      });
      this.blacklistDataSource = new MatTableDataSource<BlacklistDataSource>();
      prs.forEach(pr => {
        this.blacklistDataSource.data.push(this.createDataSource(pr));
      });
    });
   }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.socketConnection.unsubscribe();
  }

  createDataSource(product: Product): BlacklistDataSource {
     return {materialnr: product.graphQl_id, name: product.name, revenue: this.formatCurrency(product.revTotal),
      margin: this.formatCurrency(product.marginTotal), salesqnt: product.salesqtyTotal};
  }

  formatCurrency(value: number): string {
    return new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 2
    }).format(value);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.blacklistDataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.blacklistDataSource.data.forEach(row => this.selection.select(row));
  }

  onRemove() {
    const selectedItems = this.selection.selected.map(item => item.materialnr);

    this.socketConnection.next({
      method: 'deleteManyFromBlacklist',
      graphQl_ids: selectedItems
    });
  }
}
