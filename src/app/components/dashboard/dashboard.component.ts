import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from '@services/dashboard/dashboard.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  dashboardData: any;
  cardSelection: Array<any>;
  allStatistics: any;
  statSubscription: Subscription;
  onlyMyActivity: Boolean;
  activitySubscription: Subscription;

  constructor(private dashboardService: DashboardService) {
    this.activitySubscription = this.dashboardService.onlyMyActivity.subscribe((value) => {
      this.onlyMyActivity = value;
    })
    this.dashboardService.cardSelection.subscribe((newSelection) => {
      this.cardSelection = newSelection;
      this.updateCardView();
    })
    this.statSubscription = dashboardService.dashboardObservable.subscribe((statistics) => {
      this.allStatistics = statistics;
      this.updateCardView();
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.statSubscription.unsubscribe();
    this.activitySubscription.unsubscribe();
  }

  updateCardView(){
    if(this.allStatistics && this.cardSelection) {
      this.dashboardData = {
        ...this.allStatistics,
        charts: this.allStatistics.charts.filter((chart) => {
          return this.cardSelection[chart.key] && this.cardSelection[chart.key].visible;
        })
      };
    }  
  }

}
