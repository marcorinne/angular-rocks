/* angular */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

/* application */
import { ProductService } from '@services/product/product.service';
import { SocketService } from '@services/socket/socket.service';
import { Product } from '../../models/product';

@Component({
  selector: 'app-comments-sidebar',
  templateUrl: './comments-sidebar.component.html',
  styleUrls: ['./comments-sidebar.component.scss'],
  animations: [
    trigger('toggleSidebar', [
      state('true', style({
        width: '0px'
      })),
      state('false', style({
        width: '300px'
      })),
      transition('false <=> true', animate('100ms ease'))
    ])
  ]
})
export class CommentsSidebarComponent implements OnInit, OnDestroy {

  prodService: ProductService;
  socketConnection;
  product: Product;

  Object = Object;

  textField = '';
  idField = '';

  closed = true;
  animating = false;

  constructor(private productService: ProductService, private socketService: SocketService) {
    this.prodService = productService;
    productService.currentProductObservable.subscribe((product) => {
      if (product) {
        this.closed = false;
        this.product = product;
        setTimeout(() => {
          this.productService.resizeTable();
        }, 200);
      }
    });
    this.socketConnection = socketService.socketConnection;
  }

  isOwnComment(comment) {
    return comment.author === JSON.parse(localStorage.getItem('id_token_claims_obj')).name;
  }

  comment() {
    this.socketConnection.next({
      method: 'add',
      graphQl_id: this.product.graphQl_id,
      text: this.textField
    });
    this.textField = '';
  }

  deleteComment(comment_id) {
    this.socketConnection.next({
      method: 'delete',
      graphQl_id: this.product.graphQl_id,
      comment_id
    });
  }

  close() {
    this.product = null;
    this.closed = true;
    this.prodService.deleteCurrentProduct();
    setTimeout(() => {
      this.productService.resizeTable();
    }, 200);
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.socketConnection.unsubscribe();
  }

  /**
 * Start a sidebar animation.
 */
  start() {
    this.animating = true;
    this.tick();
  }

  /**
   * End a sidebar animation.
   */
  done() {
    this.animating = false;
  }

  /**
   * Perform a sidebar animation.
   */
  tick() {
    if (this.animating) {
      requestAnimationFrame(() => this.tick());
    }
  }

}
