/* angular */
import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatExpansionPanel, MatDialogConfig, MatDialog } from '@angular/material';
import { DashboardService } from '@services/dashboard/dashboard.service';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

/* application */
import { BlacklistDialogComponent } from './../recommendation/blacklist-dialog/blacklist-dialog.component';
import { ProductService } from '@services/product/product.service';
import { TreeService } from '@services/recommendations/tree.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('collapseSidebar', [
      state('true', style({
        width: '70px'
      })),
      state('false', style({
        width: '250px'
      })),
      transition('false <=> true', animate('100ms ease'))
    ])
  ]
})
export class SidebarComponent implements OnInit {

  @ViewChild('collapsePanel') collapsePanel: MatExpansionPanel;
  activitySlider = new FormControl();

  /**
   * A change in the internal state will cause the sidebar
   * to collapse or to open. A change on the external state
   * will cause the sidebar-panels to open/close depending
   * on their previous state and click events.
   * Two seperated variables are needed in order to time the
   * animations. Animation on the sidebar-panels should be
   * perfomed after the sidebar is opened and animations
   * on the sidebar should be perfomerd after the sidebar-
   * panels are closed.
   */
  internalCollapsed = true;
  externalCollapsed = true;
  animating = false;
  gridOptions;
  currentUrl: string;
  dashboardSelection: Array<any> = [];

  allColumns = true;
  includeComments = false;

  selectedGraph: string;

  constructor(private productService: ProductService,
              private dashboardService: DashboardService,
              private router: Router,
              public dialog: MatDialog,
              private treeService: TreeService) {
    this.gridOptions = productService.gridOptions;
    this.currentUrl = this.router.url;
    dashboardService.cardSelection.subscribe((selection) => {
      const selectionArray = [];
      if (Object.keys(selection).length)  {
        for (const key of Object.keys(selection)) {
          selectionArray.push({
            key,
            visible: selection[key].visible,
            nicename: selection[key].nicename
          });
        }
        this.dashboardSelection = selectionArray;
      }
    });
  }

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;
      }
      // Get the initial value of behavior subject
      this.selectedGraph = this.treeService.initialGraph;
    });
  }

  onChange() {
    this.dashboardService.toggleMyActivity(this.activitySlider.value);
  }

  toggleSidebar() {
    const collapsed = this.internalCollapsed;
    this.collapsePanel.close(); // Prevent the collapse panel from opening.
    if (collapsed) {
      this.internalCollapsed = !collapsed;
    } else {
      this.externalCollapsed = !collapsed;
    }
    setTimeout(() => {
      this.productService.resizeTable();
    }, 200);
    setTimeout(() => {
      if (collapsed) {
        this.externalCollapsed = !collapsed;
      } else {
        this.internalCollapsed = !collapsed;
      }
    }, 100); // Set the state 100ms later.
  }

  /**
   * Open the sidebar if an icon is clicked
   * and the sidebar is collapsed.
   */
  clickAccordion() {
    if (this.externalCollapsed) {
      this.toggleSidebar();
    }
  }

  /**
   * Start a sidebar animation.
   */
  start() {
    this.animating = true;
    this.tick();
  }

  /**
   * End a sidebar animation.
   */
  done() {
    this.animating = false;
  }

  /**
   * Perform a sidebar animation.
   */
  tick() {
    if (this.animating) {
      requestAnimationFrame(() => this.tick());
    }
  }

  updateColumns(event) {
    this.productService.updateColumns(event);
  }

  showBlacklist() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '850px';

    this.dialog.open(BlacklistDialogComponent, dialogConfig);
  }

  updateCards(event) {
    this.dashboardService.toggleCard(event);
  }

  export() {
    const params = {
      processCellCallback: (param) => {
        if (typeof (param.value) === 'object') {
          if (this.includeComments) {
            let comments = '';
            for (const comment of param.value) {
              comments += comment.author + ': ' + comment.text + ' | ';
            }
            return comments.slice(0, -2);
          } else {
            return param.value.length;
          }
        } else if (typeof (param.value) === 'number') {
          return param.value.toFixed(2);
        } else {
          return param.value;
        }
      },
      fileName: 'products.csv',
      allColumns: this.allColumns

    };
    this.gridOptions.api.exportDataAsCsv(params);
  }

  updateSelectedGraph(): void {
    this.treeService.updateSelectedGraph(this.selectedGraph);
  }
}
