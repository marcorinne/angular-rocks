import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-doughnut',
  templateUrl: './doughnut.component.html',
  styleUrls: ['./doughnut.component.scss']
})
export class DoughnutComponent implements OnInit {

  @Input() chartData

  public options = {
    responsive: true,
    cutoutPercentage: 70,
    legend: {
      position: 'bottom'
    }
  };

  public doughnutChartLabels = [];
  public doughnutChartData = [];
  public doughnutChartType = 'doughnut';
  public chartColors: Array<any> = [
    {
      backgroundColor: ['#3F51B5', '#1E88E5', '#00BCD4']
    }
  ]

  constructor() { }

  ngOnInit() {
    let tempData = [];
    let tempLabels = [];
    for(const key of Object.keys(this.chartData.data)){
      tempData.push(this.chartData.data[key]);
      tempLabels.push(key);
    }
    this.doughnutChartData = tempData;
    this.doughnutChartLabels = tempLabels;
  }

}
