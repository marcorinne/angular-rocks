import { Component, OnInit, Input, OnChanges, ViewChild } from '@angular/core';
import { MatExpansionPanel } from '@angular/material';

@Component({
  selector: 'app-sidebar-panel',
  templateUrl: './sidebar-panel.component.html',
  styleUrls: ['./sidebar-panel.component.scss']
})
export class SidebarPanelComponent implements OnInit, OnChanges {

  @Input() name: string;
  @Input() icon: string;
  @Input() sidebarCollapsed: boolean;

  @ViewChild('panel') panel: MatExpansionPanel;

  /**
   * Internal state whether the panel should be
   * opend or closed. Needed to remember the
   * state when the sidebar is collapsed and to
   * reopen previously opened panels.
   */
  expanded = false;

  constructor() { }

  ngOnInit() {
  }

  /**
   * Open or close the panel on sidebar
   * collapse/open events.
   */
  ngOnChanges() {
    if (this.sidebarCollapsed) {
      this.expanded = this.panel.expanded; // Save state.
      this.panel.close();
    } else if (this.expanded) {
      this.panel.open();
    }
  }

  /**
   * Methos that gets triggered whenever
   * the panel is clicked.
   */
  togglePanel() {
    /** Value whether the panel should open
     * or close. This has to be stored as
     * the this.panel.close(); will override
     * the value to false.
     */
    const expanded = this.panel.expanded;

    this.panel.close(); // Prevent the panel from opening.

    /**
     * Determine time to perform the open/close. Either
     * 100ms is the sidebar was previously closed or 1ms
     * if it was already open. 1ms is required to deal
     * with possible race conditions between .close() and
     * .open()
     */
    const time = this.sidebarCollapsed ? 100 : 1;
    setTimeout(() => {
      this.panel.expanded = expanded;
    }, time);
  }

}
