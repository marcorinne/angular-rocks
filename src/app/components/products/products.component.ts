/* angular */
import { Component, OnInit } from '@angular/core';

/* application */
import { ProductService } from '@services/product/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  rowData: any;
  gridOptions;

  constructor(private productService: ProductService) {
    this.rowData = productService.productsObservable;
    this.gridOptions = productService.gridOptions;
  }

  ngOnInit() { }

  onGridReady(params) {
    this.productService.initGrid(params);
  }

  updateColumns(event) {
    this.productService.updateColumns(event);
  }

}
