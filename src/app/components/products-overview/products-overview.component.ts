import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-products-overview',
  templateUrl: './products-overview.component.html',
  styleUrls: ['./products-overview.component.scss']
})
export class ProductsOverviewComponent implements OnInit, OnChanges {

  @Input() chartsData

  public options = {
    responsive: true,
    cutoutPercentage: 80,
    legend: {
      display: false
    },
    tooltips: {
      enabled: false
    }
  };

  public doughnutChartLabels = ["Phased-Out", "Not Phased-Out", "In Phase-Out Process"];
  public doughnutChartData = [];
  public doughnutChartType = 'doughnut';
  public chartColors: Array<any> = [
    {
      backgroundColor: ['#3F51B5','#1E88E5','#00BCD4']
    }
  ]

  constructor() { }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges){
    this.doughnutChartData = [
      this.chartsData.data.isPhasedOut,
      this.chartsData.data.isNotPhasedOut,
      this.chartsData.data.isInPhaseOutProcess
      
    ]
  }

}
