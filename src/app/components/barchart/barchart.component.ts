import { Component, OnInit, Input } from '@angular/core';
import { DashboardService } from '@services/dashboard/dashboard.service';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.scss']
})
export class BarchartComponent implements OnInit {

  public barChartOptions = {
    responsive: true,
    legend: {
      display: false
    },
    tooltips: {
      intersect: false,
      position: 'nearest'
    },
    elements: {
      rectangle: {
        borderWidth: 1
      }
    }
  };
  public barChartLabels: Array<any>;
  public barChartType = 'bar';
  public chartColors: Array<any> = [
    {
      backgroundColor: ['#3F51B5','#3F51B5','#3F51B5','#3F51B5','#3F51B5']
    }
  ]

  public barChartData = [
    {data: []}
  ];
  min: number;
  max: number;

  @Input() chartData

  constructor(private dashboardService: DashboardService) {
  }

  ngOnInit() {
    this.min = this.chartData.min;
    this.max = this.chartData.max;
    let labels = [];
    let counts = [];
    for(let bucket in this.chartData.buckets){
      const thisBucket = this.chartData.buckets[bucket];
      const thisLabel = (parseInt(bucket) === this.chartData.buckets.length -1)
        ? `> ${this.numberFormatter(thisBucket.lowerBound.toFixed(0))}`
        : ((parseInt(bucket) === 0)
            ? `< ${this.numberFormatter(thisBucket.upperBound.toFixed(0))}`
            : `[${this.numberFormatter(thisBucket.lowerBound.toFixed(0))};${this.numberFormatter(thisBucket.upperBound.toFixed(0))}]`
          );
      labels.push(thisLabel);
      counts.push(thisBucket.count);
    }
    this.barChartLabels = labels;
    this.barChartData[0].data = counts;
  }

  numberFormatter(raw) {
    const numberFormatter = new Intl.NumberFormat();
    return numberFormatter.format(raw);
  }


}
