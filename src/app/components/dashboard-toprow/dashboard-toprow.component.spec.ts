import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardToprowComponent } from './dashboard-toprow.component';

describe('DashboardToprowComponent', () => {
  let component: DashboardToprowComponent;
  let fixture: ComponentFixture<DashboardToprowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardToprowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardToprowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
