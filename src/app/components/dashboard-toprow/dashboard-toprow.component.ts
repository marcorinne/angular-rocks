import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-dashboard-toprow',
  templateUrl: './dashboard-toprow.component.html',
  styleUrls: ['./dashboard-toprow.component.scss']
})
export class DashboardToprowComponent implements OnInit, OnChanges {

  @Input() dashboardData;
  @Input() onlyMyActivity;

  recentActivities: Array<any>;
  trendingData: Array<any>;
  overview: any;

  constructor() { }

  ngOnInit() {
  }

  isOwnComment(comment) {
    return comment.author === JSON.parse(localStorage.getItem('id_token_claims_obj')).name;
  }

  getTrendColor(trend) {
    switch (trend) {
      case 'up':
        return 'primary';
      case 'down':
        return 'warn';
      case 'flat':
        return 'accent';
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.overview = this.dashboardData.overview;
    if (this.onlyMyActivity) {
      this.recentActivities = this.dashboardData.recentActivity.filter((thisActivity) => {
        return this.isOwnComment(thisActivity.comment);
      }).slice(0, 5);
    } else {
      this.recentActivities = this.dashboardData.recentActivity.slice(0, 5);
    }

    this.trendingData = this.dashboardData.trending.slice(0, 5);
  }

}
