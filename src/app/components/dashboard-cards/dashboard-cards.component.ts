import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-dashboard-cards',
  templateUrl: './dashboard-cards.component.html',
  styleUrls: ['./dashboard-cards.component.scss']
})
export class DashboardCardsComponent implements OnInit, OnChanges {

  @Input() dashboardData;

  charts: Array<any>;

  constructor() { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges){
    this.charts = this.dashboardData.charts;
  }

}
