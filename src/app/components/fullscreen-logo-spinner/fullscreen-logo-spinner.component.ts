/* angular */
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fullscreen-logo-spinner',
  templateUrl: './fullscreen-logo-spinner.component.html',
  styleUrls: ['./fullscreen-logo-spinner.component.scss']
})
export class FullscreenLogoSpinnerComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
