import { Product } from '../product';

export class RecommendationsState {
    recommendations: Product[];
    selectedRecommendation: Product;
}
