export const environment = {
  production: true,
  backendURL: 'https://phaseout-api.herokuapp.com',
  AUTH_API_PATH: 'https://auth-soley-automatic-radchuk-1773.azurewebsites.net'
};
